# Polymorphic Lookup #

Used to add polymorphism, e.g. simulate a polymorphic lookup, on an object.

Extension of custom lookup here: https://github.com/pozil/sfdc-ui-lookup-lwc#getting-started

### Components ###

* Custom polymorphic lookup
* Custom related list to display polymorphic records on record pages
* Custom view all component to mimic OOTB "view all" related list functionality

### Getting Started ###

1. Navigate to Setup > Custom Metadata Types > Polymorphic Lookup Object
2. Add custom metadata records for any object that you wish to search for with the polymorphic lookup component
	* SObject Name: API name of the Object that you wish to include in the lookup query
	* Icon: The icon that you wish to associate with records returned in the query. See https://www.lightningdesignsystem.com/icons/
	* Name Field: API name of the field you wish to designate as a Name field for records returned in the query
3. Add the Polymorphic Lookup component to the Lightning Page for the object that you wish to create polymorphic relationships for
4. Add a custom text (not long text) field with a 255 character limit called 'Related_Record_Ids__c' to the object on which you have placed the Polymorphic Lookup component
5. Add the Polymorphic Related List component to the Lightning Page for any related objects that you wish to display relationships for
	* listName: The title of the related list. Does not have to match the object label or API name
	* iconName: The icon that you wish to display in the title of the related list
	* objApiName: The API name of the related object (the object on which you have placed the Polymorphic Lookup component)
	* fieldsToDisplay: A comma-separated list of fields that you wish to have displayed in the related list. Must begin with 'Id'
	
### Additional Comments ###

* Uses SOSL to query records for which you have configured Polymorphic_Lookup_Object__mdt records for based on user input keywords
* Writes related ids to the 'Related_Record_Ids__c' on the polymorphic lookup object
	* Because the LIKE operator is not supported in long text area fields, the current maximum # of relationships on a single object is 14