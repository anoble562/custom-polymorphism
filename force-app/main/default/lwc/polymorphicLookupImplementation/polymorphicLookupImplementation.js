import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import apexSearch from '@salesforce/apex/PolymorphicLookupController.search';
import updateRelationships from '@salesforce/apex/PolymorphicLookupController.updateRelationships';
import getExistingSelections from '@salesforce/apex/PolymorphicLookupController.getExistingSelections';

export default class IntegrationLogLookup extends LightningElement {

    @api recordId;
    @api objectApiName;
    @track existingSelections = [];

    @wire(getExistingSelections, { recordId: "$recordId", objType: "$objectApiName" })
    existingData({ data, error }) {
        if (data) {
            this.existingSelections = data;
        } else if (error) {
            const event = new ShowToastEvent({
                title: 'Error fetching column data',
                variant: 'error',
                message: this.buildError(error),
            });
            this.dispatchEvent(event);
        }
    }

    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error during search',
                    variant: 'error',
                    message: this.buildError(error),
                });
                this.dispatchEvent(event);
            });
    }

    handleSelectionChange(event) {
        const selectedIds = event.detail;
        updateRelationships({ idList: selectedIds, recordId: this.recordId })
            .then(result => {
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error updating relationships',
                    variant: 'error',
                    message: this.buildError(error),
                });
                this.dispatchEvent(event);
            });
    }

    buildError(error) {
        let errorString;
        if (Array.isArray(error.body)) {
            errorString = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorString = error.body.message;
        }
        return errorString;
    }
}