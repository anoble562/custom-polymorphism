import { LightningElement, api } from 'lwc';

export default class PolymorphicRelatedListViewAll extends LightningElement {

    @api tableData = [];
    @api listName = [];
    @api columns = [];
    @api totalResults;
    @api recordLink;
    @api recordTab;
    @api pluralLabel;
    @api name;

    sortDirection = 'asc';
    sortedBy = 'Name';

    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const { fieldName: sortedBy, sortDirection } = event.detail;
        const cloneData = [...this.tableData];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.tableData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = event.detail.fieldName;
    }
}