import { LightningElement, wire, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import getData from '@salesforce/apex/PolymorphicLookupController.getRelatedListData';
import getTableColumns from '@salesforce/apex/PolymorphicLookupController.getTableColumns';
import getName from '@salesforce/apex/PolymorphicLookupController.getRecordName';

export default class PolymorphicRelatedList extends NavigationMixin(LightningElement) {

    /*** public properties ***/
    @api recordId;
    @api listName;
    @api objectApiName;
    @api objApiName;
    @api iconName;
    @api fieldsToDisplay;

    /*** datatable properties ***/
    noSortColumns = [];
    wiredTableData = [];
    @track tableData = [];
    @track displayData = [];
    @track userColumns = [];

    /*** conditional rendering properties ***/
    @track showTable = false;
    @track showViewAll = false;

    /*** other properties ***/
    listTitle;
    nameFields;
    labelPlural;
    recName;
    totalResults;

    connectedCallback() {
        this.getFieldData();
        //refreshApex(this.wiredTableData);
    }

    @wire(getData, { recordId: "$recordId", fields: "$fieldsToDisplay", objectApiName: "$objApiName" }) data(result) {
        this.wiredTableData = result;
        if (result.data) {
            let nameUrl;
            this.tableData = result.data.map(row => {
                nameUrl = `/${row.Id}`;
                return { ...row, nameUrl }
            })
            let size = result.data.length;
            this.totalResults = size;
            this.setTitle(size);
            this.displayData = this.tableData.slice(0, 5);
        } else if (result.error) {
            const event = new ShowToastEvent({
                title: 'Error fetching table data',
                variant: 'error',
                message: this.buildError(result.error),
            });
            this.dispatchEvent(event);
            this.tableData = undefined;
        }
    }

    @wire(getObjectInfo, { objectApiName: "$objectApiName" })
    sourceRecordInfo({ data, error }) {
        if (data) {
            this.nameFields = data.nameFields;
            this.labelPlural = data.labelPlural;
        } else if (error) {
            const event = new ShowToastEvent({
                title: 'Error fetching object info',
                variant: 'error',
                message: this.buildError(error),
            });
            this.dispatchEvent(event);
        }
    }

    @wire(getName, { recordId: "$recordId", nameFields: "$nameFields", objectApiName: "$objectApiName" })
    recordName({ data, error }) {
        if (data) {
            this.recName = data;
        } else if (error) {
            const event = new ShowToastEvent({
                title: 'Error fetching object name',
                variant: 'error',
                message: this.buildError(error),
            });
            this.dispatchEvent(event);
        }
    }

    getFieldData() {
        if (this.fieldsToDisplay) {
            let data = this.fieldsToDisplay.split(",");
            let userInputColumns = [];
            for (let i = 0; i < data.length; i++) {
                userInputColumns.push(data[i]);
            }
            this.getTableData(userInputColumns);
        }
    }

    getTableData(fields) {

        getTableColumns({ fields: fields, objectApiName: this.objApiName })
            .then(result => {
                this.columns = result;
                let newArray = result.map(({ sortable, ...item }) => item);
                const finalArray = newArray.map(item => ({
                    ...item,
                    hideDefaultActions: true
                }))
                this.noSortColumns = finalArray;
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error fetching column data',
                    variant: 'error',
                    message: this.buildError(error),
                });
                this.dispatchEvent(event);
            });
    }

    setTitle(size) {
        if (size > 5) {
            this.listTitle = this.listName + ' (5+)';
            this.showViewAll = true;
            this.showTable = true;
        } else if (size <= 5 && size > 0) {
            this.listTitle = this.listName + ' (' + size + ')';
            this.showTable = true;
        } else {
            this.listTitle = this.listName + ' (0)';
        }
    }

    displayViewAll() {
        let name = this.recName;
        let recordLink = '/' + this.recordId;
        let recordTab = '/lightning/o/' + this.objectApiName + '/list?filterName=Recent';
        var compDefinition = {
            componentDef: "c:polymorphicRelatedListViewAll",
            attributes: {
                listName: this.listName,
                tableData: this.tableData,
                columns: this.columns,
                totalResults: this.totalResults,
                recordLink: recordLink,
                recordTab: recordTab,
                pluralLabel: this.labelPlural,
                name: name
            }
        };
        // Base64 encode the compDefinition JS object
        var encodedCompDef = btoa(JSON.stringify(compDefinition));
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/one/one.app#' + encodedCompDef
            }
        });
    }

    buildError(error) {
        let errorString;
        if (Array.isArray(error.body)) {
            errorString = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorString = error.body.message;
        }
        return errorString;
    }
}