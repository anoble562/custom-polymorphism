@isTest(isParallel=true)
public class PolymorphicLookupControllerTest {

    @TestSetup
    static void makeData(){
        Account account = createAccount('Account');
        Opportunity oppty = createOpportunity('Oppty');

        /*** UPDATE BELOW WITH RELEVANT OBJECT WHEN IMPLEMENTING ***/
        Integration_Log__c log = new Integration_Log__c();
        log.Related_Record_Ids__c = account.Id+';'+oppty.Id;
        insert log;
    }

    @isTest
    static void search_should_return_Account() {
        List<Id> fixedResults = new Id[2];
        Account account = createAccount('Account');
        fixedResults.add(account.Id);
        Opportunity oppty = createOpportunity('Oppty');
        fixedResults.add(oppty.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();

        List<PolymorphicLookupSearchResult> results = PolymorphicLookupController.search('Acc', selectedIds);

        String type = results[0].getSObjectType();
        String icon = results[0].getIcon();
        String subtitle = results[0].getSubtitle();
        String link = results[0].getLink();

        System.assertEquals(2, results.size());
        System.assertEquals(account.Id, results.get(0).getId());
        System.assertEquals(oppty.Id, results.get(1).getId());
    }

    @isTest
    static void search_should_not_return_selected_item() {
        List<Id> fixedResults = new Id[2];
        Account account1 = createAccount('Account1');
        fixedResults.add(account1.Id);
        Account account2 = createAccount('Account2');
        fixedResults.add(account2.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();
        selectedIds.add(account2.Id);

        List<PolymorphicLookupSearchResult> results = PolymorphicLookupController.search('Acc',selectedIds);

        System.assertEquals(1, results.size());
        System.assertEquals(account1.Id, results.get(0).getId());
    }

    @isTest
    static void test_getRecordName(){
        Account a = [SELECT Id, Name FROM Account LIMIT 1];
        List<String> fields = new List<String>{'Name'};
        String objType = 'Account';

        Test.startTest();
        String name = PolymorphicLookupController.getRecordName(a.Id, fields, objType);
        Test.stopTest();

        System.assertEquals(a.Name, name);
    }

    @isTest
    static void test_updateRelationships(){
        Integration_Log__c obj = [SELECT Id FROM Integration_Log__c LIMIT 1];
        List<String> testStrings = new List<String>{'test1', 'test2', 'test3'};

        Test.startTest();
        PolymorphicLookupController.updateRelationships(testStrings, obj.Id);
        Test.stopTest();

        Integration_Log__c updatedObj = [SELECT Related_Record_Ids__c FROM Integration_Log__c WHERE Id = :obj.Id];

        System.assertEquals('test1;test2;test3', updatedObj.Related_Record_Ids__c);
    }

    @isTest
    static void test_getRelatedListData(){
        Account a = [SELECT Id FROM Account LIMIT 1];
        String fields = 'Id';
        String objType = 'Integration_Log__c';

        Test.startTest();
        List<SObject> objList = PolymorphicLookupController.getRelatedListData(a.Id, fields, objType);
        Test.stopTest();

        System.assertEquals(1,objList.size());
    }

    @isTest
    static void test_getExistingSelections(){
        Integration_Log__c obj = [SELECT Id FROM Integration_Log__c LIMIT 1];
        String objType = 'Integration_Log__c';

        Test.startTest();
        List<PolymorphicLookupSearchResult> selections = PolymorphicLookupController.getExistingSelections(obj.Id, objType);
        Test.stopTest();

        System.assertEquals(2, selections.size());
    }

    @isTest
    static void test_getTableColumns(){
        List<String> fields = new List<String>{'Id','AccountNumber','Phone','Description','NumberOfEmployees','AnnualRevenue','Type','IsDeleted','CreatedDate'};
        String objType = 'Account';

        Test.startTest();
        List<PolymorphicLookupController.TableColumn> columns = PolymorphicLookupController.getTableColumns(fields, objType);
        Test.stopTest();

        System.assertNotEquals(null, columns);
    }
    
    private static Account createAccount(String name) {
        Account account = new Account(Name = name);
        insert account;
        return account;
    }

    private static Opportunity createOpportunity(String name) {
        Opportunity oppty = new Opportunity(
        Name = name,
        StageName = 'open',
        CloseDate = Date.today()
        );
        insert oppty;
        return oppty;
    }
}
