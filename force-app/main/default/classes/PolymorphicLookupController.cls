public with sharing class PolymorphicLookupController {
    
    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(Cacheable=true)
    public static List<PolymorphicLookupSearchResult> search(String searchTerm, List<String> selectedIds) {

        searchTerm += '*';

        List<Polymorphic_Lookup_Objects__mdt> metadata = [SELECT SObject_Name__c, Icon__c, Name_Field__c FROM Polymorphic_Lookup_Objects__mdt];

        Map<String, Polymorphic_Lookup_Objects__mdt> metadataMap = new Map<String, Polymorphic_Lookup_Objects__mdt>();
        for(Polymorphic_Lookup_Objects__mdt md : metadata){
            metadataMap.put(md.SObject_Name__c, md);
        }

        String searchString = 'FIND :searchTerm IN ALL FIELDS RETURNING ';
        for(Integer i=0; i < metadata.size(); i++){
            searchString += metadata[i].SObject_Name__c+'(Id,'+metadata[i].Name_Field__c+' WHERE Id NOT IN :selectedIds)';
            if(i+1 == metadata.size()){
                searchString += ' LIMIT '+MAX_RESULTS;
            } else{
                searchString += ', ';
            }
        }

        List<List<SObject>> searchResults = search.query(searchString);
        List<PolymorphicLookupSearchResult> results = new List<PolymorphicLookupSearchResult>();

        for(List<SObject> sObjects : searchResults){
            if(!sObjects.isEmpty()){
                String sObjectType = sObjects[0].getSObjectType().getDescribe().getName();
                String icon = metadataMap.get(sObjectType).Icon__c;
                for(SObject sObj : sObjects){
                    String subtitle = sObjectType;
                    results.add(new PolymorphicLookupSearchResult(sObj.Id, sObjectType, icon, (String)sObj.get(metadataMap.get(sObjectType).Name_Field__c), subtitle, '/'+sObj.Id));
                }
            }
        }
        results.sort();
        return results;
    }

    @AuraEnabled(Cacheable = true)
    public static String getRecordName(String recordId, List<String> nameFields, String objectApiName){

        String query = 'SELECT '+String.join(nameFields, ',')+' FROM '+objectApiName+' WHERE Id = :recordId LIMIT 1';
        SObject obj = Database.query(query);

        String name;
        if(nameFields.size() > 1){
            name = (String)obj.get(nameFields[0]) + ' ' +(String)obj.get(nameFields[1]);
        } else{
            name = (String)obj.get(nameFields[0]);
        }
        return name;
    }

    @AuraEnabled
    public static void updateRelationships(List<String> idList, String recordId){

        String related;
        for(String result : idList){
            if(related == null){
                related = result;
            }else{
                related += ';'+result;
            }
        }
        Id id = (Id)recordId;
        SObject sObj = id.getSObjectType().newSObject(recordId);
        sObj.put('Related_Record_Ids__c', related);
        update sObj;
    }

    @AuraEnabled(Cacheable=true)
    public static List<SObject> getRelatedListData(Id recordId, String fields, String objectApiName){

        try {
            String search = 'SELECT Name,'+fields+' FROM '+objectApiName+' WHERE Related_Record_Ids__c LIKE \'%'+recordId+'%\' ORDER BY Name DESC';
            List<SObject> recordList = Database.query(search);
            return recordList;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static List<PolymorphicLookupSearchResult> getExistingSelections(String recordId, String objType){
        
        String objQuery = 'SELECT Related_Record_Ids__c FROM '+objType+' WHERE Id = :recordId';
        SObject contextObj = Database.query(objQuery);
        String relatedRecs = (String)contextObj.get('Related_Record_Ids__c');

        Map<String, Polymorphic_Lookup_Objects__mdt> metadataMap = buildMetadataMap();
        List<PolymorphicLookupSearchResult> resultList = new List<PolymorphicLookupSearchResult>();

        Map<String, String> idVsObjNameMap = new Map<String, String>();

        Set<Id> idSet = new Set<Id>();
        for(Id id : relatedRecs.split(';')){
            Schema.SObjectType sobjectType = id.getSObjectType();
            String sobjectName = sobjectType.getDescribe().getName();
            idVsObjNameMap.put(id, sobjectName);
            String query = 'SELECT '+metadataMap.get(sobjectName).Name_Field__c +' FROM '+sobjectName+' WHERE Id = :id';
            
            //TODO .. how to not have query in loop??
            for(SObject obj : Database.query(query)){
                String icon = metadataMap.get(idVsObjNameMap.get(obj.Id)).Icon__c;
                resultList.add(new PolymorphicLookupSearchResult(obj.Id, idVsObjNameMap.get(obj.Id), icon, (String)obj.get(metadataMap.get(sobjectName).Name_Field__c), idVsObjNameMap.get(obj.Id),'/'+obj.Id));
            }
        }
        return resultList;
    }

    @AuraEnabled(Cacheable=true)
    public static List<TableColumn> getTableColumns(List<String> fields, String objectApiName){

        SObjectType r = ((SObject)(Type.forName('Schema.'+objectApiName).newInstance())).getSObjectType();
            DescribeSObjectResult d = r.getDescribe();
            
            List<TableColumn> columnList = new List<TableColumn>();
            for(String field : fields){
                TableColumn column = new TableColumn();
                column.label = d.fields.getMap().get(field).getDescribe().getLabel();
                column.fieldName = field;
                column.sortable = true;

                String type = String.valueOf(d.fields.getMap().get(field).getDescribe().getType());

                switch on type.toLowerCase() {
                    when 'id' {
                        column.type = 'url';
                        column.label = 'Name';
                        column.fieldName = 'nameUrl';
                
                        TypeAttribute attributes = new TypeAttribute();
                        AttributeLabel atLabel = new AttributeLabel();
                        atLabel.fieldName = 'Name';
                        attributes.label = atLabel;
                        attributes.target = '_blank';
                        column.typeAttributes = attributes;
                    }
                    when 'string', 'picklist', 'combobox', 'textarea', 'reference' {
                        column.type = 'text';  
                    }
                    when 'date', 'datetime'{
                        column.type = 'date';
                        TypeAttribute attributes = new TypeAttribute();
                        attributes.year = 'numeric';
                        attributes.month = 'long';
                        attributes.day = '2-digit';
                        attributes.hour = '2-digit';
                        attributes.minute = '2-digit';
                        column.typeAttributes = attributes;
                    }
                    when 'integer', 'double', 'long', 'decimal' {
                        column.type = 'number';
                    }
                    when 'percent' {
                        column.type = 'percent';
                        //Integer digits = d.fields.getMap().get(field).getDescribe().getScale();
                    }
                    when 'currency' {
                        column.type = 'currency';
                    }
                    when 'boolean' {
                        column.type = 'boolean';
                    }
                    when 'phone' {
                        column.type = 'phone';
                    }
                    when 'email' {
                        column.type = 'email';
                    }
                    when else {
                        column.type = 'text';
                    }
                }
                columnList.add(column);
            }
        return columnList;
    }

    private static Map<String, Polymorphic_Lookup_Objects__mdt> buildMetadataMap(){

        List<Polymorphic_Lookup_Objects__mdt> metadata = [SELECT SObject_Name__c, Icon__c, Name_Field__c FROM Polymorphic_Lookup_Objects__mdt];

        Map<String, Polymorphic_Lookup_Objects__mdt> metadataMap = new Map<String, Polymorphic_Lookup_Objects__mdt>();
        for(Polymorphic_Lookup_Objects__mdt md : metadata){
            metadataMap.put(md.SObject_Name__c, md);
        }
        return metadataMap;
    }

    public class TableColumn {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String type {get;set;}
        @AuraEnabled
        public String fieldName {get;set;}
        @AuraEnabled
        public Boolean sortable {get;set;}
        @AuraEnabled
        public TypeAttribute typeAttributes {get;set;}
    }
    public class TypeAttribute {
        @AuraEnabled
        public AttributeLabel label {get;set;}
        @AuraEnabled
        public String target {get;set;}
        //public String step {get;set;}
        @AuraEnabled
        public String year {get;set;}
        @AuraEnabled
        public String month {get;set;}
        @AuraEnabled
        public String day {get;set;}
        @AuraEnabled
        public String hour {get;set;}
        @AuraEnabled
        public String minute {get;set;}
    }
    public class AttributeLabel {
        @AuraEnabled
        public String fieldName {get;set;}
    }
}