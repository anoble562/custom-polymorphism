/**
 * Class used to serialize a single Lookup search result item
 * The Lookup controller returns a List<LookupSearchResult> when sending search result back to Lightning
 */

 //src: https://github.com/pozil/sfdc-ui-lookup-lwc/tree/master/src/main/default/classes
 
public virtual class PolymorphicLookupSearchResult implements Comparable {
    public Id id;
    protected String sObjectType;
    protected String icon;
    protected String title;
    protected String subtitle;
    protected String link;

    protected PolymorphicLookupSearchResult() {
    }

    public PolymorphicLookupSearchResult(Id id, String sObjectType, String icon, String title, String subtitle, String link) {
        this.id = id;
        this.sObjectType = sObjectType;
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.link = link;
    }

    @AuraEnabled
    public Id getId() {
        return id;
    }

    @AuraEnabled
    public String getSObjectType() {
        return sObjectType;
    }

    @AuraEnabled
    public String getIcon() {
        return icon;
    }

    @AuraEnabled
    public String getTitle() {
        return title;
    }

    @AuraEnabled
    public String getSubtitle() {
        return subtitle;
    }
    @AuraEnabled
    public String getLink() {
        return link;
    }

    /**
     * Allow to sort search results based on title
     */
    public Integer compareTo(Object compareTo) {
        PolymorphicLookupSearchResult other = (PolymorphicLookupSearchResult) compareTo;
        if (this.getTitle() == null) {
            return (other.getTitle() == null) ? 0 : 1;
        }
        if (other.getTitle() == null) {
            return -1;
        }
        return this.getTitle().compareTo(other.getTitle());
    }
}